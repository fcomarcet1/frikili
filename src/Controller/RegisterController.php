<?php

namespace App\Controller;

use App\Entity\User;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Form\UserType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class RegisterController
 * @package App\Controller
 */
class RegisterController extends AbstractController {


    /**
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
	{

		$user = new User();
		$form = $this->createForm(UserType::class, $user);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) 
		{
			// $form->getData() holds the submitted values
			// but, the original `$user` variable has also been updated
			$user = $form->getData();

			$user->setPassword($passwordEncoder->encodePassword($user, $user->getPassword()));
			$user->setRoles(["ROLE_USER"]);
			$user->setBanned(false);
			$user->setCreatedAt(new DateTime());
			$user->setUpdatedAt(new DateTime());
			//var_dump($user);die();

			// Saving the user to the database
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            //Flash message register Success
			$this->addFlash('success', User::REGISTER_SUCCESS_MESSAGE);

			return $this->redirectToRoute('register');
		}

		return $this->render('register/register.html.twig', [
					'controller_name' => 'RegisterController',
					'form' => $form->createView(),
		]);
	}

}
