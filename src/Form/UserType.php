<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class UserType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
		
		
		$builder->add('fullName', TextType::class, [
					'label' => 'Nombre:',
					'required' => true,
				])
				->add('email', EmailType::class, [
					'label' => "Email:",
					'required' => true,
				])
				->add('password', PasswordType::class, [
					'label' => "Contraseña:",
					'required' => true,
				])
				->add('submit', SubmitType::class,[
					'label'=>"Registro"
				])
		;
	}

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
		$resolver->setDefaults([
			'data_class' => User::class,
			
//			// enable/disable CSRF protection for this form
//            'csrf_protection' => true,
//            // the name of the hidden HTML field that stores the token
//            'csrf_field_name' => '_token',
//            // an arbitrary string used to generate the value of the token
//            // using a different string for each form improves its security
//            'csrf_token_id'   => 'task_item',
		]);
	}

}
